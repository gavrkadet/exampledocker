$('td').on('click', function () {
    if (!$(this).hasClass('clicked')) {
        $(this).addClass('clicked')
        $.ajax({
            type: 'POST',
            url: 'http://game/',
            data: {
                cord: $(this).attr('data-cord'),
            },
            dataType: 'JSON',
            success: function (data) {
                console.log('success')
                $('#target').html(data);
            },
            error: function (data) {
                console.log('error')
                $('#target').html(data);
            }
        });
    } else {
        $(this).removeClass('clicked')
    }
})
