<?php

/** @var yii\web\View $this */

$this->title = 'Game';

$rows = 30; // количество строк, tr
$cols = 30; // количество столбцов, td

$table = '<table>';

for ($tr = 1; $tr <= $rows; $tr++) {
    $table .= "<tr>";
    for ($td = 1; $td <= $cols; $td++) {
        $table .= "<td data-cord='{$td}:{$tr}'>";
        redis()->set($tr . ':' . $td, 'false');
    }
}

//dd(redis()->get('1:1'));
$table .= '</table>';
echo $table; // сделали эхо всего 1 раз

if (isset($_POST['cord'])) {
    redis()->set($_POST['cord'], '');
}
?>

<script src="https://code.jquery.com/jquery-latest.js"></script>

