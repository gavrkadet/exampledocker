<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

class DebugController extends Controller
{
    public function actionIndex()
    {
        $arr = [];
        for ($y = 0; $y < 30; $y++) {
            for ($x = 0; $x < 30; $x++) {
                $arr[] = $y . ':' . $x;
            }
        }
        //dd(redis()->mget(...$arr));
        dd(array_combine($arr, redis()->mget(...$arr)));
    }
}