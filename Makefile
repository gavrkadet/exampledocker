up:
	docker compose up --force-recreate --remove-orphans --build # --detach
up-detach:
	docker compose up --force-recreate --remove-orphans --build --detach
php-sh:
	docker compose exec game-php-fpm sh
nginx-sh:
	docker compose exec game-nginx sh

